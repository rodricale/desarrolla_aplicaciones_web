**Desarrollo de Aplicaciones Web con conexiones a base de datos**
**Alumno:**
*López Osuna Jesus Rodrigo*
**Semestre:**
*5AVP*


-Practica #1 - 02/09/2022 - Practica_ejemplo
commit: 1564652fef2784f5fe09bee2265c70e1e263806e
Archivo: https://gitlab.com/rodricale/desarrolla_aplicaciones_web/-/blob/main/parcial1/practica_ejemplo.html

-Práctica #2 - 09/09/2022 - Práctica JavaScript
commit: 90d4b7908dabb0cb247fc48b3a801984e9ba50f2
Archivo: https://gitlab.com/rodricale/desarrolla_aplicaciones_web/-/blob/main/parcial1/practicaJavaScript.html

-Práctica #3 - 15/09/2022 - Práctica Web con base de datos - Parte 1
commit: dfeb82c06d66bfe6d088d61d9341625a253ae02f
Archivo: https://gitlab.com/rodricale/desarrolla_aplicaciones_web/-/blob/main/parcial1/PracticaWebApp.rar

-Práctica #4 - 19/09/2022 - Consultar Datos
commit: facbcfcc593871a3f3d4120195d090321a2a2aa5
Archivo: https://gitlab.com/rodricale/desarrolla_aplicaciones_web/-/blob/main/parcial1/consultarDatos.php

-Práctica #6 - 26/09/2022 - Práctica Web conexión
commit: aeae25467e0de36d1be68c47ded2f22c83734f10
Archivo: https://gitlab.com/rodricale/desarrolla_aplicaciones_web/-/blob/main/parcial1/conexion.php
